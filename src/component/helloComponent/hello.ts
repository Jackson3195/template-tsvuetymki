import Vue from "vue";
import { Component, Data, Lifecycle } from "vuety";

import "./hello.scss";

@Component({
    template: require("./hello.html")
})
export default class HelloComponent extends Vue {
    @Data public name: String;

    @Lifecycle protected created() {
        this.name = "Jackson Jacob";
    }
}
