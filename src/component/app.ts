import Vue from "vue";
import { Component, Data, Lifecycle } from "vuety";
import HelloComponent from "./helloComponent/hello";

@Component({
  template: require("./app.html"),
  components: {"hello": HelloComponent}
})
export default class App extends Vue {
  // Data property
  @Data public myDataProperty: string = "";

  // Lifecycle hook
  @Lifecycle protected mounted () {
    this.myDataProperty = "Mounted";
  }

  // Component method
  protected updateMyProperty ($event: any) {
    this.myDataProperty = $event.target.value;
  }
}
