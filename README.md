# VueJS + TypeScript + Vuety

> Template project setup with VueJS 2, TypeScript, WebPack 2 and Vuety.

Build Vue JS 2 apps using TypeScript in single file components.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# When importing Vue, 'vue' -> Full Build, 'Vue' -> Runtime ONLY build
import Vue from 'vue' (Full)
import Vue from 'Vue' (Runtime only build)

# build for production with minification
npm run build
```


